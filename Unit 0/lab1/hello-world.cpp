/* hello-world.cpp
    Now including Block Comments! */
    
#include <iostream>

int main() {
    std::cout << "Hello World! ";               // Prints "Hello World!" using std::cout
    std::cout << "I'm a C++ program ";          // Prints more using the same method
    printf("I'm a C command in a C++ program"); // Prints string using a C operator
}
