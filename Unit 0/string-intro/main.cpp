// my first string
#include <iostream>
#include <string>
using namespace std;

int main()
{
    string mystring;
    mystring = "This is the initial string comment";
    cout << mystring << endl;
    mystring = "This is a different string comment";
    cout << mystring << endl;
    return 0;
}
