#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

// Global declaration of methods
void start();
void intro();
void clear();
void login();
void createAccount();
void printHeader();
void menu(int u);

// Global declaration of variables
bool accountCreated = false;
double balance0, balance1, balance2, pocket0, pocket1, pocket2;
int account0, account1, account2, pin0, pin1, pin2;
string name0, name1, name2, current_username;

// Starting method, sets all variables to their initial correct values, and runs the intro screen.
void start(){
    account0 = 0;
    account1 = 0;
    account2 = 0;
    pin0 = 0;
    pin1 = 0;
    pin2 = 0;
    balance0 = 100;
    balance1 = 100;
    balance2 = 100;
    pocket0 = 100;
    pocket1 = 100;
    pocket2 = 100;

    intro();
}

// Waits for the user to press return.
void pause(){
    string strtmp;
    cout << "\n\nPress any key, then RETURN to continue.\n";  
    cin >> strtmp;
}

// Clears the screen.
void clear(){
    system("clear");
}

// Prints a specified number of dashes for a nicer header.
void printHeader(const char *x){
    int i = strlen(x);    // Checks the length of the string
        for(i; i>0; i--){
            printf("-");
        }
    printf("\n");
    printf("%s\n", x);
    i = strlen(x);    // Checks the length of the string
        for(i; i>0; i--){
            printf("-");
        }
    printf("\n\n");
}

// Intro screen. Allows for creation of accounts, logging in, and quitting.
void intro(){
    clear();
    int choice;
    printHeader("Welcome to the Bank of Corelto!");
    cout << "Choose an option:\n1. Login\n2. Create new account\n3. Quit\n\nEnter your choice: ";
    cin >> choice;
    switch (choice)
    {
    case 1:
        login();
        break;
    case 2:
        createAccount();
        break;
    case 3:
        exit(0);
        break;
    }
}

// Account creation
void createAccount(){
    int new_acct, new_pin;
    clear();
    printHeader("Create a Bank of Corleto account:");
    if(!accountCreated){
        printf("There are no existing accounts. Please\nproceed to create an account to use\nthe banking system.\n\n");
    }
    printf("Welcome to the account creation wizard.\nHere, you can create up to 3 accounts, and\ncorresponding pin numbers.");
    pause();
    clear();
    printHeader("Create a Bank of Corleto account:");
    if(account0 == 0){  // Checks if slot 0 is open
        printf("Hello, User 1. Please enter your first name.\nThis name cannot include spaces.\n\nEnter your first name: ");    // Takes in account holder's data
        cin >> name0;
        cout << "\nHello, " << name0 << ". Please create an User ID.\nThis number can not be zero.\n\nEnter your User ID: ";
        cin >> account0;
        new_acct = account0;
        printf("\nGreat! Now, create a pin number. This number\nis the key to your account, and should be\ntreated with care.\n\nEnter your pin code: ");
        cin >> pin0;
        new_pin = pin0;
    } else if (account1 == 0){ // If slot 0 is closed, is slot 1 open?
        printf("Hello, User 2. Please enter your first name.\nThis name cannot include spaces.\n\nEnter your first name: ");
        cin >> name1;
        cout << "\nHello, " << name1 << ". Please create an User ID.\nThis number can not be zero.\n\nEnter your User ID: ";
SELUID1:
        cin >> account1;
        if(account1 == account0){ // Checks for duplicate account numbers.
            printf("This User ID is taken. Please choose another User ID number.\n\nEnter your User ID: ");
            goto SELUID1;
        }
        new_acct = account1;
        printf("\nGreat! Now, create a pin number. This number\nis the key to your account, and should be\ntreated with care.\n\nEnter your pin code: ");
        cin >> pin1;
        new_pin = pin1;
    } else if (account2 == 0){ // If slot 0 and 1 are closed, is slot 2 open?
        printf("Hello, User 3. Please enter your first name.\nThis name cannot include spaces.\n\nEnter your first name: ");
        cin >> name2;
        cout << "\nHello, " << name2 << ". Please create an User ID.\nThis number can not be zero.\n\nEnter your User ID: ";
        cin >> account2;
SELUID2:
        if(account2 == account0 || account2 == account1){   // Checks for duplicate account numbers.
            printf("This User ID is taken. Please choose another User ID number.\n\nEnter your User ID: ");
            goto SELUID2;
        }
        new_acct = account2;
        printf("\nGreat! Now, create a pin number. This number\nis the key to your account, and should be\ntreated with care.\n\nEnter your pin code: ");
        cin >> pin2;
        new_pin = pin2;
    } else {
        printf("The Bank of Corleto is currently not accepting\nnew accounts at this time. Please check\nagain in a few days.");
        pause();
        login();
    }
    clear();
    printHeader("Success! Account created.");
    printf("Now that your account has been created, you can\nstart widthdrawing and depositing money. As\na welcome bonus, your account has been credited\nwith an initial balance of $100. Spend it\nwisely, and thanks for choosing the Bank of Corleto!\n\nAccount details: your User ID is %d and your pin number is %d.", new_acct, new_pin);
    accountCreated = true;  // Tells the login screen not to forward directly to the account creation screen. 
    pause();
    intro();
}

void login(){
    int user_check, pin_check, authenticated_user;
    clear();
    printHeader("Log into your Bank of Corleto account:");
    if(accountCreated){ // Checks if accounts exist, if not, forward to creation wizard.
ENTERID:
        printf("Enter your User ID number: ");
        cin >> user_check;
        if(user_check == account0){
            authenticated_user = 0; // Selects the current set of data.
            current_username = name0;
            cout << "\nHello " << name0 << ". Please enter your pin to\nconfirm your identity:\n\nEnter your pin number: ";
AUTH0:
            cin >> pin_check;
            if(pin_check == pin0){
                printf("\nUser successfully authenticated.");
                pause();
            } else {
                printf("\nThe pin you entered doesn't match the\naccount. Please try again.\n\nEnter your pin number: ");
                goto AUTH0;
            }
        } else if(user_check == account1){
            authenticated_user = 1;
            current_username = name1;
            cout << "\nHello " << name1 << ". Please enter your pin to\nconfirm your identity:\n\nEnter your pin number: ";
AUTH1:
            cin >> pin_check;
            if(pin_check == pin1){
                printf("\nUser successfully authenticated.");
                pause();
            } else {
                printf("\nThe pin you entered doesn't match the\naccount. Please try again.\n\nEnter your pin number: ");
                goto AUTH1;
            }
        } else if(user_check == account2){
            authenticated_user = 2;
            current_username = name2;
            cout << "\nHello " << name2 << ". Please enter your pin to\nconfirm your identity:\n\nEnter your pin number: ";
AUTH2:
            cin >> pin_check;
            if(pin_check == pin2){
                printf("\nUser successfully authenticated.");
                pause();
            } else {
                printf("\nThe pin you entered doesn't match the\naccount. Please try again.\n\nEnter your pin number: ");
                goto AUTH2;
            }
        } else {
            clear();
            printHeader("Log into your Bank of Corleto account:");
            printf("User ID not recognized. Try again?\n\n");
            goto ENTERID;
        }
        menu(authenticated_user);   // Forwards authenticated user to correct account values in menu.
    } else {
        createAccount();   // If there are no accounts, make one.
    }
}

void menu(int u){   // Takes in user id (internal)
    int choice;
    clear();
    printHeader("Main Menu");
    if(u = 0){  // Prints correct balance
        cout << "Hello " << current_username << ".\nYour account balance is: $" << balance0 << ".\nYou have $" << pocket0 << " in your pocket.";
    } else if (u = 1){
        cout << "Hello " << current_username << ".\nYour account balance is: $" << balance1 << ".\nYou have $" << pocket1 << " in your pocket.";
    } else if (u = 2){
        cout << "Hello " << current_username << ".\nYour account balance is: $" << balance2 << ".\nYou have $" << pocket2 << " in your pocket.";
    }

    cout << "\n\nChoose an option:\n1. Make a deposit\n2. Make a widthdrawal\n3. Go back to the login screen.\n\nEnter your choice: "; // Prompts for action
    cin >> choice;
    switch (choice)
    {
    case 1:
        clear();
        printHeader("Deposit");
DEPOSIT:    
        double deposit;
        if(u = 0){ // Shows balance (again)
            cout << "Your account balance is: $" << balance0 << ".\nYou have $" << pocket0 << " in your pocket.";
        } else if (u = 1){
            cout << "Your account balance is: $" << balance1 << ".\nYou have $" << pocket1 << " in your pocket.";
        } else if (u = 2){
            cout << "Your account balance is: $" << balance2 << ".\nYou have $" << pocket2 << " in your pocket.";
        }
        cout << "\n\nHow much would you like to deposit? $";
        cin >> deposit;
        if(u = 0){  // Selects account
            if(deposit <= pocket0){ // Math for funds transfer
                pocket0 = pocket0 - deposit;
                balance0 = balance0 + deposit;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto DEPOSIT;
            }
        } else if(u = 1){
            if(deposit <= pocket1){
                pocket1 = pocket1 - deposit;
                balance1 = balance1 + deposit;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto DEPOSIT;
            }
        } else if(u = 2){
            if(deposit <= pocket2){
                pocket2 = pocket2 - deposit;
                balance2 = balance2 + deposit;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto DEPOSIT;
            }
        }
        break;
    case 2:
        clear();
        printHeader("Widthdraw");   // Same as deposit with flipped signs
WIDTHDRAW:    
        double widthdraw;
        if(u = 0){  // Selects account and shows balance
            cout << "Your account balance is: $" << balance0 << ".\nYou have $" << pocket0 << " in your pocket.";
        } else if (u = 1){
            cout << "Your account balance is: $" << balance1 << ".\nYou have $" << pocket1 << " in your pocket.";
        } else if (u = 2){
            cout << "Your account balance is: $" << balance2 << ".\nYou have $" << pocket2 << " in your pocket.";
        }
        cout << "\n\nHow much would you like to widthdraw from your account? $";
        cin >> widthdraw;
        if(u = 0){
            if(widthdraw <= pocket0){
                pocket0 = pocket0 + widthdraw;
                balance0 = balance0 - widthdraw;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto WIDTHDRAW;
            }
        } else if(u = 1){
            if(widthdraw <= pocket1){
                pocket1 = pocket1 + widthdraw;
                balance1 = balance1 - widthdraw;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto WIDTHDRAW;
            }
        } else if(u = 2){
            if(widthdraw <= pocket2){
                pocket2 = pocket2 + widthdraw;
                balance2 = balance2 - widthdraw;
                menu(u);
            } else {
                cout << "Insufficient funds.\n";
                goto WIDTHDRAW;
            }
        }
        break;
    case 3:
        intro();
        break;
    }

}

int main() {
    start(); // Runs start method.
}
