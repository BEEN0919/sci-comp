// Quadratic Formula Project
// @author Sean O'Connor
#include <iostream>
#include <tgmath.h> // Math library for square root

using namespace std;

int main()
{
    double a, b, c, r1, r2, d, lat, real; // Defining all terms in the formula
    cout << "Enter term 'a':";            // Taking input for all terms needed
    cin >> a;
    cout << "Enter term 'b':";
    cin >> b;
    cout << "Enter term 'c:";
    cin >> c;

    // Calculation for both terms
    d = (b * b) - (4 * a * c); // Calculate all terms under the radical, known as the discriminant

    // Logic for imaginary or single roots:
    if (d == 0) // If the discriminant is 0, there is only 1 root.
    {
        r1 = (-b) / (2 * a); // Calculate the formula disregarding the discriminant, because \sqrt(0) = 0.
        cout << "There is only 1 root. The root is " << r1 << "." << endl;
    }
    else if (d > 0) // If the discriminant is greater than 0: calculate normally.
    {
        r1 = (-b) + sqrt(d); // Calculate -b + or - the square root of the discriminant
        r1 = r1 / (2 * a);   // Divide numerator by denominator
        r2 = (-b) - sqrt(d); // Repeat for 2nd term
        r2 = r2 / (2. * a);
        cout << "Root 1 is " << r1 << endl;
        cout << "Root 2 is " << r2 << endl;
    }
    else
    {                             // If the discriminant is negative, the radical is negative and does not compute to a real number.
        real = (-b) / (2 * a);    // Calculate the real number to the left of the imaginary.
        lat = sqrt(-d) / (2 * a); // Calculate the imaginary part of the result (lateral). Negating the radical because you cannot take the square root of a negative.
        if (lat == 1)             // Detects if the lateral is 1
        {
            // If the lateral is 1, print out just the i.
            cout << "Root 1 is " << real << " + " << "i" << endl;
            cout << "Root 2 is " << real << " - " << "i" << endl;
        }
        else
        {
            // If the lateral isn't 1, print normally.
            cout << "Root 1 is " << real << " + " << lat << "i" << endl;
            cout << "Root 2 is " << real << " - " << lat << "i" << endl;
        }
    }

    return 0;
}