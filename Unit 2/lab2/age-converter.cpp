// age-converter.cpp
#include "stdafx.h"

using namespace std;

int main()
{
	double ageInYears;

	cout << "Enter your age in years: ";
	cin >> ageInYears;

	double ageInSeconds = ((((ageInYears * 365) * 24) * 60) * 60);

	cout.imbue(std::locale(""));
	cout << fixed << setprecision(2);

	cout << "Age (years) = "
		 << ageInYears << endl;

	cout << "Age (secs) = "
		 << ageInSeconds << endl;

	return 0;
}
